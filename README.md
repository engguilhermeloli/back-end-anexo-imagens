# Desafio Controlle - Backend

Esse projeto tem como intuito fornecer as rotas necessárias para a conclusão do desafio.

#### Como rodar o projeto

Para rodar o projeto basta dar um `git clone` e após isso instalar as dependências com `npm install`. Ao receber a mensagem que a instalação foi concluída basta startar o servidor utilizando `npm run dev`, se estiver tudo ok, irá aparecer uma mensagem `🚀 Server ready at http://localhost:3000` 

#### Rotas atuais

Atualmente o servidor está setado para rodar em `localhost:3000` caso houver algum problema essa porta pode ser alterada em `./src/index.js`.

##### Upload
Method: POST   
Path: http://localhost:3000/api/v1/attachments   
Body: ```{ "base64" : "...", "name": "imagem", "type": "image/jpeg" }```

*Para o método upload é necessário que os valores do body sejam enviados como FormData.*


**Retornos possíveis:**   
[STATUS] 200
`{
    "message": "Adicionado com sucesso"
}`   
[STATUS] 500
`{
    "message": <MENSAGEM DE ERRO>
}`

##### Get
Method: GET   
Path: http://localhost:3000/api/v1/attachments

*Esse método retornará a lista de seus uploads contendo o base64 de cada um.*

**Retornos possíveis:**   
[STATUS] 200
`{
    "values": [...]
}`   
[STATUS] 500
`{
    "message": <MENSAGEM DE ERRO>
}`

import storage from 'node-persist';
import { v4 as uuidv4 } from 'uuid';

const KEY_STORAGE = 'files';
const PATH_STORAGE = 'storage';

const parseValues = (values) => (values ? JSON.parse(values) : []);

const getValues = async () => {
  await storage.init({
    dir: PATH_STORAGE,
    stringify: JSON.stringify,
    parse: JSON.parse,
    encoding: 'utf8',
    logging: false,
    ttl: false,
    expiredInterval: 2 * 60 * 1000,
    forgiveParseErrors: false
  });

  const values = await storage.getItem(KEY_STORAGE);

  return parseValues(values);
};

const upload = async ({ name, base64, type }) => {
  await storage.init({
    dir: PATH_STORAGE,
    stringify: JSON.stringify,
    parse: JSON.parse,
    encoding: 'utf8',
    logging: false,
    ttl: false,
    expiredInterval: 2 * 60 * 1000,
    forgiveParseErrors: false

  });

  const values = await getValues();

  await storage.setItem(KEY_STORAGE, JSON.stringify(values.concat({
    key: uuidv4(),
    base64,
    type,
    name
  })));
};

const removeImage = async ({ key }) => {
  await storage.init({
    dir: PATH_STORAGE,
    stringify: JSON.stringify,
    parse: JSON.parse,
    encoding: 'utf8',
    logging: false,
    ttl: false,
    expiredInterval: 2 * 60 * 1000,
    forgiveParseErrors: false
  });

  const values = await getValues();

  const image = values.find((item) => item.key === key);

  values.splice(values.indexOf(image), 1);

  storage.setItem(KEY_STORAGE, JSON.stringify(values));
};

const deleteImage = async (req, res) => {
  try {
    const { key } = req.params;

    await removeImage({ key });

    res.status(200).json({ message: 'Removido com sucesso.' });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const post = async (req, res) => {
  try {
    const { base64, name, type } = req.fields;

    await upload({ base64, name, type });

    res.status(200).json({ message: 'Adicionado com sucesso' });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const get = async (req, res) => {
  try {
    const values = await getValues();

    res.status(200).json({ values });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export {
  post,
  get,
  deleteImage
};

import express from 'express';
import { post, get, deleteImage } from './attachments';

const app = express();

const router = express.Router();

router.post('/attachments', post);
router.get('/attachments', get);
router.delete('/attachments/:key', deleteImage);

app.use('/', router);

export default app;
